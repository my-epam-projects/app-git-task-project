package org.gradle;


import org.gradle.exceptions.InvalidInputException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TestStringUtils {

    @Test()
    void testIsPositive() throws InvalidInputException {
        assertTrue(StringUtils.isPositiveNumber("123"));
        assertFalse(StringUtils.isPositiveNumber("-456"));
    }

    @Test()
    void testWithInvalidValue() {
        InvalidInputException thrown = Assertions.assertThrows(InvalidInputException.class, () -> {
            StringUtils.isPositiveNumber("asd");
            StringUtils.isPositiveNumber("*-++");
        });
        Assertions.assertEquals("invalid input value", thrown.getMessage());
    }
}
