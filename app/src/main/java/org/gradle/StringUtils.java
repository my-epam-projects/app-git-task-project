package org.gradle;


import org.gradle.exceptions.InvalidInputException;

public class StringUtils {
    public static boolean isPositiveNumber(String str) throws InvalidInputException {
        try {
            if (Integer.parseInt(str) > 0){
                return org.apache.commons.lang3.StringUtils.isNumeric(str);
            }
            return false;
        }catch (NumberFormatException e){
            throw new InvalidInputException("invalid input value");
        }
    }

    private StringUtils(){

    }
}
